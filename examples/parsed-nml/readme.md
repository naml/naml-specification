# Parsed NML

NML is parsed using the [NML parser](https://gitlab.com/naml/parser)

its parsed into a generic c struct

```C
struct uiObject
{
    int width;
    int height;
    uiObject children[];
    colour textcolour;
    colour backgroundcolour;
    int x;
    int y;
    int z;
    const char* id;
    const char* type;
    const char* class[];
}
```

Some tags get parsed into a different object that has a generic `uiObject` as property

```C
struct imgObject
{
    uiObject base;
    const char* source;
}
```

```C
struct rootObject
{
    style styles[];
    const char* title;
    uiObject rootUiObject;
}
```
